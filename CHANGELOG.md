# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2020-05-14
### Added
- Ajout de fonctions dans le fichier functions.php pour améliorer le wordpress de base.

## [0.2.0] - 2020-05-14
### Added
- Installation et configuration de [TGM Plugin Activation]
- Ajout de [WP Mail SMTP] en tant que plugin par défaut


## [0.1.0] - 2020-05-13
### Added
- Installation de [Laravel-Mix]
- installation et configuration de [Tailwindcss]
- Mise en place du minimum d'un thème [Wordpress]

[Laravel-Mix]: https://laravel-mix.com/
[Tailwindcss]: https://tailwindcss.com/
[Wordpress]: https://developer.wordpress.org/themes/getting-started/
[TGM Plugin Activation]: https://github.com/TGMPA/TGM-Plugin-Activation
[0.1.0]: https://gitlab.com/deshiloh/wordpress-theme-boilerplate/-/tags/0.1.0
[0.2.0]: https://gitlab.com/deshiloh/wordpress-theme-boilerplate/-/tags/0.2.0
[0.3.0]: https://gitlab.com/deshiloh/wordpress-theme-boilerplate/-/tags/0.3.0
[WP Mail SMTP]: https://fr.wordpress.org/plugins/wp-mail-smtp/