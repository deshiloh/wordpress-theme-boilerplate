<?php

require_once "vendor/tgmpa/tgm-plugin-activation/class-tgm-plugin-activation.php";
require 'inc/plugins-required-config.php';

function add_theme_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	wp_enqueue_style( 'maincss', get_template_directory_uri() . '/dist/css/app.css', array(), '1.0', 'all' );

	wp_enqueue_script( 'script', get_template_directory_uri() . '/dist/js/app.js', array(), 1.0, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		)
	);

	load_theme_textdomain( 'hybrideConseil' );

	// Add support for responsive embeds.
	add_theme_support( 'responsive-embeds' );
}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );