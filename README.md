# Wordpress Theme Boilerplate

Permet la mise en place rapide de la base d'un thème Wordpress.

Ce projet utilise : 
- [Tailwindcss]
- [Laravel-Mix]
- [TGM Plugin Activation]

## Installation

``` 
composer create-project deshiloh/wordpress-boilerplate
npm install
npm run production
```

## Conseils

- Ne pas oublier de renseigner le domain dans le fichier ```style.css``` qui se trouve à la racine du projet.
- Ajout ce code dans le fichier wp-config.php (racine du projet wordpress) : 
```
define('DISALLOW_FILE_EDIT', true);
```

## Plugins par défaut
 
- [WP MAIL SMTP]

[WP MAIL SMTP]: https://fr.wordpress.org/plugins/wp-mail-smtp/ 
[Tailwindcss]: https://tailwindcss.com/
[Laravel-Mix]: https://laravel-mix.com/
[TGM Plugin Activation]: https://github.com/TGMPA/TGM-Plugin-Activation